package com.zuitt.activity;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "09123456789", "New York City");
        Contact contact2 = new Contact("Bob Marley", "090298767324", "Jamaica City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        if(phonebook.getContacts().size() == 0) {
            System.out.println("Phonebook is empty");
        } else {
            for(int i = 0; phonebook.getContacts().size() > i; i++) {
                System.out.println("Contact: " + (i + 1));
                System.out.println("Name: " + phonebook.getContacts().get(i).getName());
                System.out.println("Contact Number: " + phonebook.getContacts().get(i).getContactNumber());
                System.out.println("Address: " + phonebook.getContacts().get(i).getAddress());
            }
        }

    }
}

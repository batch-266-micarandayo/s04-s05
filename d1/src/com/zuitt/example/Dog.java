package com.zuitt.example;

public class Dog extends Animal{

    private String breed;

    public Dog() {
        super(); /*Animal() constructor*/
        this.breed = "Aspin";
    }

    public Dog(String name, String color, String breed) {
        super(name, color); /*Animal(String name, String color)*/
        this.breed = breed;
    }

    /*getters*/
    public String getBreed(){
        return this.breed;
    }

    /*Methods*/
    public void speak() {
        super.call(); /*The call() method from Animal Class*/
        System.out.println("Woof Woof!");
    }
}

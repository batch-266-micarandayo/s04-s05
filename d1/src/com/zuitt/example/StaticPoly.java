package com.zuitt.example;

public class StaticPoly {
    /*Polymorphism - the ability of code process to take many form
    *
    * 1. Static - manually constructed.
    * 2. Dynamic - automatic construction/modified in the process
    * */

    public int addition(int a, int b) {
        return a + b;
    }

    /*Overload by changing the number of arguments*/
    public int addition(int a, int b, int c) {
        return a + b + c;
    }

    public double addition(double a, double b) {
        return a + b;
    }
}
